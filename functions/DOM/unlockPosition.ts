import { MouseEvent } from 'react';

/**
 * the Component to which this function is attached to should not have any
 * absolute, relative or fixed position in his parent elements
 */
export const unlockPosition = (e: any) => {
  const el = e.currentTarget;
  const rect = el.getBoundingClientRect();

  el.style.position = 'absolute';
  // el.style.left = `${rect.x}px`;
  // el.style.top = `${rect.y}px`;
  el.style.zIndex = '1';
  el.style.margin = '0';
  el.style.padding = '0';
}