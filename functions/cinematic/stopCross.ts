import ISpeed from '../../models/ISpeed';
import { ICrossRedirection } from './crossRedirect';



const stopCross = (red: ICrossRedirection, speed: ISpeed) => {

  if (
    red.y &&
    red.y > 0 &&
    speed.y < 10 && speed.y > -10 &&
    speed.x < 1 && speed.x > -1
  ) {
    return true;
  }

  return false;
}

export default stopCross;