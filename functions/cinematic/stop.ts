
import { IExtendedCol } from '../../models/ICollision';
import ICollisionInfos from '../../models/ICollisionInfos';
import { HitboxType } from '../../models/IPosition';
import ISpeed from '../../models/ISpeed';
import { IVector } from '../../models/IVector';
import { ICrossRedirection } from './crossRedirect';
 

const stop = (
  behavior: HitboxType,
  collidedSegmentRad: number,
  speed: ISpeed,
  vector: IVector,
  gravitySpeed: number,
  eCollision: IExtendedCol,
): boolean => {
  const alpha = collidedSegmentRad;

  if (gravitySpeed === 0) return speed.y < 10 && speed.y > -10 &&
    speed.x < 1 && speed.x > -1;

  return eCollision.position.types.includes(HitboxType.Glue)
    ||
    (
      speed.y < 10 && speed.y > -10 &&
      speed.x < 1 && speed.x > -1 &&
      eCollision.position.y >= eCollision.vector.y1 &&
      eCollision.position.y >= vector.y1 &&
      (
        alpha >= 0 && alpha <= Math.PI / 4
        ||
        alpha >= 3 * Math.PI / 4 && alpha <= 5 * Math.PI / 4
        ||
        alpha >= 7 * Math.PI / 4
      )
    );
}

export default stop;