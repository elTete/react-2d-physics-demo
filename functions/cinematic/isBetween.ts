import { IVector } from '../../models/IVector';



const xIsBetween = (x: number, seg: IVector) => {
  const highest = seg.x1 >= seg.x2 ? seg.x1 : seg.x2;
  const lowest = seg.x1 <= seg.x2 ? seg.x1 : seg.x2;

  return x > lowest && x < highest;
}

export default xIsBetween;