import { IPosition } from '../../models/IPosition';
import { IExtendedV, IVector } from '../../models/IVector';


const getNorm = (v: IVector) => {
  const square = (x: number) => Math.pow(x, 2);

  return Math.sqrt(
    square(v.x2 - v.x1) + square(v.y2 - v.y1)
  );
}

export const getNormXY = (v: IVector) => {
  return {
    x: v.x2 - v.x1,
    y: v.y2 - v.y1,
  }
}

export const getExtendedV = (v: IVector): IExtendedV => {

  return {
    ...v,
    norm: getNorm(v),
    normXY: getNormXY(v),
  };
}

export const initializeVector = (): IVector => {

  return {
    x1: 0,
    y1: 0,
    x2: 0,
    y2: 0,
  }
}

export default getNorm;