import IHitbox from '../../models/IHitbox';
import getHitboxSegments from './getHitboxSegments';


const getLowestPoint = (hb: IHitbox) => {
  const segments = getHitboxSegments(hb);

  return segments.reduce((acc, seg) => {
    if (seg.y1 > acc.point && seg.y1 === seg.y2) return {
      point: seg.y1,
      seg,
    }
    // if (seg.y2 > acc.point) return {
    //   point: seg.y2,
    //   seg,
    // }

    return acc;
  }, {
    point: segments[0].y1,
    seg: segments[0],
  });
}

export default getLowestPoint;