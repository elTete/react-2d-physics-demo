import { IVector } from '../../models/IVector';

// TODO: delete if slope coef isn't needed
// const getSlopeCoef = (vector: IVector) => {

//   return (vector.y2 - vector.y1) / (vector.x2 - vector.x1);
// }

const getOrigin0Vector = (AB: IVector) => {
  return {
    x: AB.x2 - AB.x1,
    y: AB.y2 - AB.y1,
  };
}

const getAngle = (
  AB: IVector,
  CD: IVector,
  isABVector = false,
  isCDVector = false,
) => {
  const localAB: IVector = isABVector ? AB : {
    x1: AB.x1 > AB.x2 ? AB.x2 : AB.x1,
    y1: AB.y1 > AB.y2 ? AB.y2 : AB.y1,
    x2: AB.x1 > AB.x2 ? AB.x1 : AB.x2,
    y2: AB.y1 > AB.y2 ? AB.y1 : AB.y2,
  };
  const localCD: IVector = isCDVector ? CD : {
    x1: CD.x1 > CD.x2 ? CD.x2 : CD.x1,
    y1: CD.y1 > CD.y2 ? CD.y2 : CD.y1,
    x2: CD.x1 > CD.x2 ? CD.x1 : CD.x2,
    y2: CD.y1 > CD.y2 ? CD.y1 : CD.y2,
  };

  const square = (x: number) => Math.pow(x, 2);
  const u = getOrigin0Vector(localAB);
  const v = getOrigin0Vector(localCD);

  const uv = u.x * v.x + u.y * v.y;
  const uvDet = u.x * v.y - u.y * v.x;
  const uvNorm = Math.sqrt(square(u.x) + square(u.y))
    *
    Math.sqrt(square(v.x) + square(v.y));
  
  const cosTeta = uv / uvNorm;
  const teta = Math.sign(uvDet) === 1 ? Math.PI * 2 - Math.acos(cosTeta) : Math.acos(cosTeta);

  return teta;
}

export default getAngle;