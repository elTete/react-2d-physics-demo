import ICollision from '../../models/ICollision';
import { HitboxType, IPosition } from '../../models/IPosition';

const getHitTypes = (col: ICollision, mcType: HitboxType) => {

  if (col.position.types.length) return col.position.types;
  if (col.hitboxTypes.length) return col.hitboxTypes;
  return [mcType];
}

const delta = (c: ICollision, dir: IPosition) => {
  let x = 0, y = 0;
  const x2supx1 = c.vector.x2 > c.vector.x1;
  const y2supy1 = c.vector.y2 > c.vector.y1;

  if (dir.x === 1) {
    x = x2supx1 ? c.vector.x2 - c.position.x : c.vector.x1 - c.position.x;
  }
  else if (dir.x === -1) {
    x = x2supx1 ? c.vector.x1 - c.position.x : c.vector.x2 - c.position.x;
  }
  else {
    x = 0;
  }
  
  if (dir.y === 1) {
    y = y2supy1 ? c.vector.y2 - c.position.y : c.vector.y1 - c.position.y;
  }
  else if (dir.y === -1) {
    y = y2supy1 ? c.vector.y1 - c.position.y : c.vector.y2 - c.position.y;
  }
  else {
    y = 0;
  }
  
  return { x, y };
}

const redirectCrossHB = (n: IPosition, cols: ICollision[], mcType: HitboxType) => {
  const dir = {
    x: Math.sign(n.x),
    y: Math.sign(n.y),
  };

  return cols.reduce((
    acc: { x: number, y: number, types: HitboxType[], col: ICollision },
    c,
  ) => {
    const d = delta(c, dir);

    return {
      x: Math.abs(d.x) > Math.abs(acc.x) ? d.x : acc.x,
      y: Math.abs(d.y) > Math.abs(acc.y) ? d.y : acc.y,
      types: Math.abs(d.x) + Math.abs(d.y) > Math.abs(acc.x) + Math.abs(acc.y) ? getHitTypes(c, mcType) : acc.types,
      col: c,
    };

  }, { x: 0, y: 0, types: [], col: cols[0] });
}

export default redirectCrossHB;