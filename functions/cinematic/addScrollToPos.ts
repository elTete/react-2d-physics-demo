import { IPosition } from '../../models/IPosition';



const addScrollToPos = (pos: IPosition, scroll?: IPosition): IPosition => {
  if (!scroll) return pos;

  return {
    x: Math.sign(pos.x) === -1 ? pos.x + scroll.x : pos.x + scroll.x,
    y: Math.sign(pos.y) === -1 ? pos.y + scroll.y : pos.y - scroll.y,
  };
}

export default addScrollToPos;