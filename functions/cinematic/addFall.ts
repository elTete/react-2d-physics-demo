import { IPosition } from '../../models/IPosition';
import ISpeed from '../../models/ISpeed';
import ISpeedAcc from '../../models/ISpeedAcc';


const addFall = (
  position: IPosition,
  speed: ISpeed,
  speedAcc: ISpeedAcc,
  date: number,
  gravityOn: boolean,
  frictionOn: boolean,
  ground?: boolean,
) => {
  const coef = (Date.now() - date) / 40;
  const groundF = ground ? 4 : 1;
  const xF = !frictionOn ? 0 : speed.x * coef * groundF/10;
  const yF = !frictionOn ? 0 : speed.y * coef * 1/10;
  const gravity = gravityOn ? -8 * coef : 0;
  const fallVector = {
    x1: position.x,
    y1: position.y,
    x2: position.x + speed.x * coef,
    y2: position.y + speed.y * coef,
  }

  let newXSpeed = Math.round(speed.x) === 0 ?
    0 :
    speed.x - xF;
  let newYSpeed = speed.y - yF;

  return {
    vector: fallVector,
    speed: {
      x: newXSpeed,
      y: newYSpeed,
    },
    coef,
    gravity,
  };
};


export default addFall;