import ICloseCollider from '../../models/ICloseCollider';
import ICollision from '../../models/ICollision';
import IHitbox from '../../models/IHitbox';
import { HitboxType, IPosition } from '../../models/IPosition';
import ISpeed from '../../models/ISpeed';
import { IVector } from '../../models/IVector';
import getHitboxSegments from './getHitboxSegments';
import { getNormXY } from './getNorm';
import getVectorsCrossPoints from './getVectorsCrossPoints';
import nextHitboxFromV from './nextHitboxFromV';
import redirectCrossHB from './redirectCrossHB';

export interface ICrossRedirection extends IPosition {
  types: HitboxType[],
  col: ICollision,
}

const getUpLeft = (hb: IHitbox) => {
  return hb.positions.reduce((acc, pos) => {
    if (pos.x <= acc.x && pos.y <= acc.y) return pos;
    return acc;
  }, hb.positions[0]);
}

const getUpLeftDot = (seg: IVector) => {
  if (seg.x1 <= seg.x2 && seg.y1 <= seg.y2) return {
    x: seg.x1,
    y: seg.x2,
  };
  return {
    x: seg.x2,
    y: seg.y2,
  }
}

const getUpRight = (hb: IHitbox) => {
  return hb.positions.reduce((acc, pos) => {
    if (pos.x >= acc.x && pos.y <= acc.y) return pos;
    return acc;
  }, hb.positions[0]);
}

const getUpRightDot = (seg: IVector) => {
  if (seg.x1 >= seg.x2 && seg.y1 <= seg.y2) return {
    x: seg.x1,
    y: seg.x2,
  };
  return {
    x: seg.x2,
    y: seg.y2,
  }
}

// TODO: replace gravity by a function which resets gravity if gravityOn
const crossRedirect = (
  v: IVector,
  hitbox: IHitbox,
  closeColliders: ICloseCollider[],
  vRedirected: IVector,
  mcType: HitboxType,
  mcKey: string,
  gravity: number,
) => {
  const futureHitbox = nextHitboxFromV({
    x1: v.x1,
    y1: v.y1,
    x2: vRedirected.x2,
    y2: vRedirected.y2,
  }, hitbox);

  const crossed = getVectorsCrossPoints(closeColliders, getHitboxSegments(futureHitbox), mcKey, true);
  const vRedNormXY = getNormXY(vRedirected);
  let redirection = redirectCrossHB(vRedNormXY, crossed, mcType);
  if (redirection.x && redirection.y) {
    const xUpdateHB = nextHitboxFromV({
      x1: vRedirected.x2,
      y1: vRedirected.y2,
      x2: vRedirected.x2 - redirection.x,
      y2: vRedirected.y2,
    }, futureHitbox);
    const xHBCrossed = getVectorsCrossPoints(closeColliders, getHitboxSegments(xUpdateHB), mcKey, true);
    const yUpdateHB = nextHitboxFromV({
      x1: vRedirected.x2,
      y1: vRedirected.y2,
      x2: vRedirected.x2,
      y2: vRedirected.y2 - redirection.y,
    }, futureHitbox);
    const yHBCrossed = getVectorsCrossPoints(closeColliders, getHitboxSegments(yUpdateHB), mcKey, true);
    if (!yHBCrossed.length) {
      redirection = { ...redirection, x: 0 };
    } else if (!xHBCrossed.length) {
      redirection = { ...redirection, y: 0 };
    }
  }
  const types = redirection.types;

  // if (types.includes(HitboxType.Bounce)) {

  //   return {
  //     v: {
  //       ...vRedirected,
  //       x2: vRedirected.x2 - redirection.x,
  //       y2: vRedirected.y2 - redirection.y,
  //     },
  //     speed: {
  //       x: 0,
  //       y: -20 * Math.sign(redirection.y),
  //     },
  //     types,
  //     redirection,
  //   };
  // }

  if (types.includes(HitboxType.Normal)) {

    return {
      v: {
        ...vRedirected,
        x2: vRedirected.x2 - redirection.x,
        y2: vRedirected.y2 - redirection.y,
      },
      speed: { x: null, y: redirection.y ? gravity : null },
      types,
      redirection,
    };
  }

  return {
    v: vRedirected,
    speed: { x: null, y: null },
    types,
    redirection,
  };
}

export default crossRedirect;