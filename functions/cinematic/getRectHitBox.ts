import IHitbox from '../../models/IHitbox';
import { HitboxType, IPosition } from '../../models/IPosition';


/**
 * 
 * @param key 
 * @param position 
 * @param width 
 * @param height 
 * @returns 
 */
const getRectHitBox = (
  key: string,
  types: HitboxType[],
  position: IPosition,
  width: number,
  height: number,
): IHitbox => {

  return {
    types,
    positions: [
      {
        x: position.x - width/2,
        y: position.y - height/2,
        types: [],
      },
      {
        x: position.x + width/2,
        y: position.y - height/2,
        types: [],
      },
      {
        x: position.x + width/2,
        y: position.y + height/2,
        types: [],
      },
      {
        x: position.x - width/2,
        y: position.y + height/2,
        types: [],
      },
    ],
    key,
  }
}

export default getRectHitBox;