
import ICloseCollider from '../../models/ICloseCollider';
import IHitbox from '../../models/IHitbox';
import { HitboxType } from '../../models/IPosition';
import ISpeed from '../../models/ISpeed';
import { IVector } from '../../models/IVector';
import applyVectorToHitbox from './applyVectorToHitbox';
import { getNormXY } from './getNorm';
import redirect from './redirect';
import stop from './stop';
import updateHitbox from './updateHitbox';
import getVectorsCrossPoints from './getVectorsCrossPoints';
import crossRedirect from './crossRedirect';
import stopCross from './stopCross';
import ICollision from '../../models/ICollision';

interface IHit {
  vector: IVector,
  speed: ISpeed,
  stopped: boolean,
  hits: number,
  hitbox: IHitbox,
  types: HitboxType[],
  col?: ICollision,
}

// TODO: funtions have too many arguments, refactoring needed
const addHit = (
  vector: IVector,
  speed: ISpeed,
  closeColliders: ICloseCollider[],
  key: string,
  hitbox: IHitbox,
  gravitySpeed: number,
  behavior: HitboxType,
) => {

  const actualCCs = closeColliders.filter(
    cc => cc.movingColliderKey === key
  );

  let hit = true, stopped = false;
  let hits = 0;
  let result: IHit[] = [];
  let types: HitboxType[] = [];

  while (hit && !stopped) {
    hit = false;
    const lastHit = hits > 0 ? result[hits - 1] : null;
    const lastVector = lastHit ? lastHit.vector : vector;
    const lastSpeed = lastHit ? lastHit.speed : speed;
    const lastHitbox = lastHit ? lastHit.hitbox : hitbox;

    let hitboxV: IVector | null;
    if (!lastHit) hitboxV = null
    else if (hits === 1) {
      hitboxV = {
        x1: vector.x1,
        y1: vector.y1,
        x2: lastVector.x1,
        y2: lastVector.y1,
      }
    } else {
      hitboxV = {
        x1: result[hits - 2].vector.x1,
        y1: result[hits - 2].vector.y1,
        x2: lastVector.x1,
        y2: lastVector.y1,
      }
    }

    const updatedHitbox = hitboxV ? updateHitbox(lastHitbox, hitboxV) : hitbox;
    const mcVectors = applyVectorToHitbox(updatedHitbox, getNormXY(lastVector));

    const collisions = getVectorsCrossPoints(actualCCs, mcVectors, key);

    const redirectInfos = redirect(
      lastVector,
      lastSpeed,
      gravitySpeed,
      collisions,
      behavior,
    );
  
    if (!redirectInfos.eCollision) {

      const cr = crossRedirect(
        vector, hitbox, actualCCs, redirectInfos.vector, behavior, key, gravitySpeed
      );
      const crSpeed = {
        x: cr.speed.x ? cr.speed.x : lastSpeed.x,
        y: cr.speed.y ? cr.speed.y : lastSpeed.y,
      };
      stopped = stopCross(cr.redirection, crSpeed);

      result = [
        ...result,
        {
          // vector,
          // speed: lastSpeed,
          vector: cr.v,
          speed: crSpeed,
          hits,
          stopped,
          hitbox: updatedHitbox,
          types: cr.types,
          col: cr.redirection.col
        },
      ];
      continue;
    }

    let col: ICollision = redirectInfos.eCollision;

    const vRed = redirectInfos.types.includes(HitboxType.Normal) ? vector : redirectInfos.vector;
    let cr = crossRedirect(
      vector, hitbox, actualCCs, vRed, behavior, key, gravitySpeed
    );
    
    if (cr.redirection.x || cr.redirection.y) col = cr.redirection.col;

    stopped = stop(
      behavior,
      // TODO: do something about this cast
      (redirectInfos.collidedSegmentRad as number),
      redirectInfos.speed,
      lastVector,
      gravitySpeed,
      redirectInfos.eCollision,
    );

    const crSpeed = {
      x: cr.speed.x ? cr.speed.x : redirectInfos.speed.x,
      y: cr.speed.y ? cr.speed.y : redirectInfos.speed.y,
    };
    if (!stopped) stopped = stopCross(cr.redirection, crSpeed);

    if (cr.types.includes(HitboxType.Normal)) {
      types = types.concat(cr.types);
      result = [
        ...result,
        {
          vector: cr.v,
          speed: crSpeed,
          hits,
          stopped,
          hitbox: updatedHitbox,
          types: cr.types,
          col,
        }
      ];
      continue;
    }
  
    hits++;
    hit = true;
  
    if (stopped) {
      types = types.concat(redirectInfos.types);
      result = [
        ...result,
        {
          vector: {
            ...redirectInfos.vector,
            x2: redirectInfos.vector.x1,
            y2: redirectInfos.vector.y1,
          },
          speed: { x: 0, y: 0 },
          hits,
          stopped,
          hitbox: updatedHitbox,
          types: redirectInfos.types,
          col,
        }
      ];
      continue;
    }

    types = types.concat(redirectInfos.types);
    result = [
      ...result,
      {
        vector: redirectInfos.vector,
        speed: redirectInfos.speed,
        hits,
        stopped,
        hitbox: updatedHitbox,
        types: redirectInfos.types,
        col,
      },
    ];

  }

  return {result, types};

};

export default addHit;