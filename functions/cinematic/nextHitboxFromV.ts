import IHitbox from '../../models/IHitbox';
import { IVector } from '../../models/IVector';
import { getNormXY } from './getNorm';



const nextHitboxFromV = (v: IVector, hitbox: IHitbox): IHitbox => {
  const normXY = getNormXY(v);
  return {
    ...hitbox,
    positions: hitbox.positions.map(pos => {
      return {
        x: pos.x + normXY.x,
        y: pos.y + normXY.y,
        types: pos.types,
      };
    }),
  };
}

export default nextHitboxFromV;