
import { HitboxType, IHitboxPos } from '../../models/IPosition';
import { ISplitVector, IVector } from '../../models/IVector';
import ICollision, { IExtendedCol } from '../../models/ICollision';
import { getExtendedV } from './getNorm';


const initializeTrespasserV = (v: IVector, pos: IHitboxPos) => {

  return getExtendedV({
    x1: pos.x,
    y1: pos.y,
    x2: v.x2,
    y2: v.y2,
  });
}

const initializeRightfulV = (v: IVector, pos: IHitboxPos) => {
  
  return getExtendedV({
    x1: v.x1,
    y1: v.y1,
    x2: pos.x,
    y2: pos.y,
  });
}

const getHitTypes = (col: ICollision, mcType: HitboxType) => {

  if (col.position.types.length) return col.position.types;
  if (col.hitboxTypes.length) return col.hitboxTypes;
  return [mcType];
}

/**
 * check hit exceptions
 * @param v 
 * @param col 
 * @param colTypes 
 * @returns 
 */
const checkCol = (v: IVector, col: ICollision, colTypes: HitboxType[]) => {

  if (colTypes.includes(HitboxType.Bounce)) {
    if (
      col.position.x === col.vector.x1 &&
      col.position.y === col.vector.y1
    ) {
      return false;
    }
  }

  // if (colTypes.includes(HitboxType.Normal)) {
  //   if (
  //     col.position.y === col.vector.y1 &&
  //     col.position.y < v.y1
  //   ) {
  //     return false;
  //   }
  // }

  return true;
}

/**
 * 
 * @param collisions 
 * @param mcType 
 * @param prevHitType 
 * @returns
 */
const getExtendedCol = (
  v: IVector,
  collisions: ICollision[],
  mcType: HitboxType,
): IExtendedCol | null => {

  if (!collisions.length) return null;

  const initTypes = getHitTypes(collisions[0], mcType);
  const initCol = checkCol(v, collisions[0], initTypes) ?
    {
      ...collisions[0],
      vSplitted: {
        tresspasser: initializeTrespasserV(
          collisions[0].vector,
          collisions[0].position,
        ),
        rightful: initializeRightfulV(
          collisions[0].vector,
          collisions[0].position,
        ),
      },
      hitTypes: initTypes,
    }
    : null;

  return collisions.reduce((acc: IExtendedCol | null, col, i) => {

    if (i === 0) return acc;

    const hitTypes = getHitTypes(col, mcType);
    if (!checkCol(v, col, hitTypes)) return acc;

    const vSplitted: ISplitVector = {
      tresspasser: initializeTrespasserV(col.vector, col.position),
      rightful: initializeRightfulV(col.vector, col.position),
    }

    if (!acc) return {
      ...col,
      vSplitted,
      hitTypes,
    }

    if (vSplitted.rightful.norm >= acc.vSplitted.rightful.norm) {

      return acc;
    }

    return {
      ...col,
      vSplitted,
      hitTypes,
    };
    
  }, initCol);



}


export default getExtendedCol;
