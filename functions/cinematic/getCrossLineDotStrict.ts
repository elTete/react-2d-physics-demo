import { HitboxType, IHitboxPos } from '../../models/IPosition';
import { IHitboxVec, IVector } from '../../models/IVector';

interface IRes {
  x: number | null;
  y: number | null;
  types: HitboxType[],
}


// TODO: refacto, too long
const getCrossLineDotStrict = (segmentAB: IVector, segmentCD: IHitboxVec) => {
  let x = null;
  let y = null;
  const res: IRes = {
    x: null,
    y: null,
    types: segmentCD.types,
  }

  const xA = segmentAB.x1;
  const yA = segmentAB.y1;
  const xB = segmentAB.x2;
  const yB = segmentAB.y2;

  const xC = segmentCD.x1;
  const yC = segmentCD.y1;
  const xD = segmentCD.x2;
  const yD = segmentCD.y2;

  const roundTo5th = (num: number) => Math.round(num * 100000);

  const ABIsVertical = roundTo5th(xB) - roundTo5th(xA) === 0;
  const ABIsHorizontal = roundTo5th(yB) - roundTo5th(yA) === 0;

  const CDIsVertical = roundTo5th(xD) - roundTo5th(xC) === 0;
  const CDIsHorizontal = roundTo5th(yD) - roundTo5th(yC) === 0;

  // in this case there is no cross
  if (
    ABIsVertical && CDIsVertical
    ||
    ABIsHorizontal && CDIsHorizontal
  ) {
    return {
      x,
      y,
    };
  }

  if (ABIsVertical) x = xA;
  if (CDIsVertical) x = xC;
  if (ABIsHorizontal) y = yA;
  if (CDIsHorizontal) y = yC;

  if (x === null && y === null) { // == neither vertical nor horizontal line
    x = (
      (-xA * (yA - yB)) / (xB - xA)
      - yA
      + (xC * (yC - yD)) / (xD - xC)
      + yC
      - (
        (yB - yA) * (xD - xC)
        + (yC - yD) * (xB - xA)
      ) / (
        (xB - xA) * (xD - xC)
      )
    );
  
    y = ((x - xA) * (yB - yA)) / (xB - xA) + yA;
  } else {
    if (ABIsVertical && y === null && x !== null) {
      const yDot = ((x - xC) * (yD - yC)) / (xD - xC) + yC;
      if (
        (
          yDot > yA && yDot < yB
          ||
          yDot < yA && yDot > yB
        ) && (
          x > xC && x < xD
          ||
          x < xC && x > xD
        )
      ) y = yDot;
    }
    if (CDIsVertical && y === null && x!== null) {
      const yDot = ((x - xA) * (yB - yA)) / (xB - xA) + yA;
      if (
        (
          yDot > yC && yDot < yD
          ||
          yDot < yC && yDot > yD
        ) &&
        (
          x > xA && x < xB
          ||
          x < xA && x > xB
        )
      ) y = yDot;
    }
    if (ABIsHorizontal && x === null && y !== null) {
      const xDot = (y - yC) * (xD - xC) / (yD - yC) + xC;
      if (
        (
          xDot > xA && xDot < xB
          ||
          xDot < xA && xDot > xB
        ) && (
          y > yC && y < yD
          ||
          y < yC && y > yD
        )
      ) x = xDot;
    }
    if (CDIsHorizontal && x === null && y !== null) {
      const xDot = (y - yA) * (xB - xA) / (yB - yA) + xA;
      if (
        (
          xDot > xC && xDot < xD
          ||
          xDot < xC && xDot > xD
        ) &&
        (
          y > yA && y < yB
          ||
          y < yA && y > yB
        )
      ) x = xDot;
    }
  }

  if (
    ABIsVertical && x && y
    &&
    (
      y > yA && y < yB
      ||
      y < yA && y > yB
    ) && (
      x > xC && x < xD
      ||
      x < xC && x > xD
    )
  ) {
    res.x = x;
    res.y = y;
  }

  if (
    CDIsVertical && x && y
    &&
    (
      y > yC && y < yD
      ||
      y < yC && y > yD
    ) &&
    (
      x > xA && x < xB
      ||
      x < xA && x > xB
    )
  ) {
    res.x = x;
    res.y = y;
  }

  if (
    ABIsHorizontal && x && y
    &&
    (
      x > xA && x < xB
      ||
      x < xA && x > xB
    ) && (
      y > yC && y < yD
      ||
      y < yC && y > yD
    )
  ) {
    res.x = x;
    res.y = y;
  }

  if (
    CDIsHorizontal && x && y
    &&
    (
      x > xC && x < xD
      ||
      x < xC && x > xD
    ) &&
    (
      y > yA && y < yB
      ||
      y < yA && y > yB
    )
  ) {
    res.x = x;
    res.y = y;
  }

  return res;

}

export default getCrossLineDotStrict;