import IVector0 from '../../models/IVector0';


const sohcahtoa = (
  rad: number,
  coef: number,
  vector: IVector0,
) => {

  const square = (x: number) => Math.pow(x, 2);

  const norm = Math.sqrt(
    square(vector.x)
    +
    square(vector.y)
  ) * coef;

  return {
    opposite: Math.sin(rad) * norm,
    adjacent: Math.cos(rad) * norm,
  };
}

export default sohcahtoa;