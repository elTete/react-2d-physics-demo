import IHitbox from '../../models/IHitbox';
import { IVector } from '../../models/IVector';


const updateHitbox = (hitbox: IHitbox, vector: IVector): IHitbox => {
  const norm = {
    x: vector.x2 - vector.x1,
    y: vector.y2 - vector.y1,
  }

  return {
    ...hitbox,
    positions: hitbox.positions.map(pos => {
      return {
        x: pos.x + norm.x,
        y: pos.y + norm.y,
        types: pos.types,
      }
    }),
  }
}

export default updateHitbox;