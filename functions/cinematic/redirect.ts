
import { HitboxType } from '../../models/IPosition';
import ISpeed from '../../models/ISpeed';
import { IVector } from '../../models/IVector';
import IVector0 from '../../models/IVector0';
import ICollision, { IExtendedCol } from '../../models/ICollision';
import addBounce from './addBounce';
import addNormalCollision from './addNormalCollision';
import getAngle from './getAngle';
import getExtendedCol from './getExtendedCol';

const untransposeVector = (offsets: IVector0, vector: IVector) => {
  return {
    x1: vector.x1 - offsets.x,
    y1: vector.y1 - offsets.y,
    x2: vector.x2 - offsets.x,
    y2: vector.y2 - offsets.y,
  }
}

const redirect = (
  vector: IVector,
  speed: ISpeed,
  gravitySpeed: number,
  collisions: ICollision[],
  behavior: HitboxType,
): {
  vector: IVector,
  speed: ISpeed,
  hits: boolean,
  eCollision?: IExtendedCol,
  collidedSegmentRad?: number,
  untransposedVector?: IVector,
  types: HitboxType[];
} => {

  const eCollision: IExtendedCol | null = getExtendedCol(vector, collisions, behavior);

  if (!eCollision) return {
    vector,
    speed,
    hits: false,
    types: [],
  };

  const trueBehavior = eCollision.hitTypes;

  // radian between moving object's vector and collided segment
  const teta = getAngle(eCollision.vector, eCollision.segment, true);
  // radian between x axis and collided segment
  const alpha = getAngle(eCollision.segment, { x1: 0, y1: 0, x2: 100, y2: 0 });

  // the x and y norms of the vector's part exceeding the collided segment
  const vectorToRedirectNorm: IVector0 = {
    x: eCollision.vector.x2 - eCollision.position.x,
    y: eCollision.vector.y2 - eCollision.position.y,
  }
  // const deltaX = collisionInfos.vector.x2 - collisionInfos.position.x;
  // const deltaY = collisionInfos.vector.y2 - collisionInfos.position.y;

  /**
   * vector is at the center of the moving object
   * while collisionInfos.vecor is its transposition on a vertex of the object
   * offset/delta is needed to transpose the redirected vector back to the center
   */
  const xOffset = vector.x1 - eCollision.vector.x1;
  const yOffset = vector.y1 - eCollision.vector.y1;
  const offsets = {
    x: xOffset,
    y: yOffset,
  }

  /**
   * initialized to the Moving Object's vector part that exceeds the colliding segment
   * and transposed back to the MO's center from one of its vertexes
   */
  const newVector: IVector = {
    x1: eCollision.position.x + xOffset,
    y1: eCollision.position.y + yOffset,
    x2: vector.x1 + Math.abs(eCollision.vSplitted.rightful.normXY.x),
    y2: vector.y1 + Math.abs(eCollision.vSplitted.rightful.normXY.y),
  };

  // const newSpeed: ISpeed = {
  //   x: speed.x,
  //   y: speed.y,
  // };

  if (trueBehavior.includes(HitboxType.Bounce)) {
    const bounce = addBounce(
      teta,
      alpha,
      newVector,
      speed,
      vectorToRedirectNorm,
    );

    return {
      ...bounce,
      hits: true,
      eCollision,
      collidedSegmentRad: alpha,
      untransposedVector: untransposeVector(offsets, bounce.vector),
      types: trueBehavior,
    }
  }

  if (trueBehavior.includes(HitboxType.Glue)) {
    const resVector = {
      ...newVector,
      x2: newVector.x1,
      y2: newVector.y1,
    }
    return {
      vector: resVector,
      speed: { x: 0, y: 0 },
      hits: true,
      eCollision,
      collidedSegmentRad: alpha,
      untransposedVector: untransposeVector(offsets, resVector),
      types: trueBehavior,
    }
  }

  // last case should be : behavior === HitboxType.Normal
  if (trueBehavior.includes(HitboxType.Normal)) {
    const normalCollision = addNormalCollision(
      teta,
      alpha,
      newVector,
      speed,
      eCollision.vSplitted.tresspasser.normXY,
      gravitySpeed,
    );
  
    return {
      ...normalCollision,
      hits: true,
      eCollision,
      collidedSegmentRad: alpha,
      untransposedVector: untransposeVector(offsets, normalCollision.vector),
      types: trueBehavior,
    };
  }

  return {
    vector,
    speed,
    hits: false,
    types: trueBehavior,
  };
}

export default redirect;