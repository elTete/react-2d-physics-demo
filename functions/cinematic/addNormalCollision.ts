import ISpeed from '../../models/ISpeed';
import { IVector } from '../../models/IVector';
import IVector0 from '../../models/IVector0';
import sohcahtoa from './sohcahtoa';


const addNormalCollision = (
  movingColliderRad: number,
  collidedSegmentRad: number,
  vector: IVector,
  speed: ISpeed,
  vectorToRedirectNorm: IVector0,
  gravitySpeed: number,
): {
  vector: IVector,
  speed: ISpeed,
} => {
  const alpha = collidedSegmentRad;
  const alphaReversed = (alpha + Math.PI ) % (Math.PI * 2);
  const rad = movingColliderRad;
  const coef = Math.abs(rad % Math.PI - Math.PI / 2) / (Math.PI / 2);

  let newRad: number;
  if (rad > Math.PI) {
    newRad = rad % Math.PI > Math.PI / 2 ?
      alpha : alphaReversed;
  } else {
    newRad = rad > Math.PI / 2 ?
      alphaReversed : alpha;
  }
  
  const { opposite, adjacent } = sohcahtoa(newRad, coef, vectorToRedirectNorm);

  return {
    vector: {
      ...vector,
      x2: vector.x1 + adjacent,
      y2: vector.y1 + opposite,
    },
    speed: {
      x: vectorToRedirectNorm.x === 0 ?
        0 : speed.x * adjacent / vectorToRedirectNorm.x,
      y: vectorToRedirectNorm.y === 0 ?
        gravitySpeed : speed.y * opposite / vectorToRedirectNorm.y,
    },
  };
}

export default addNormalCollision;