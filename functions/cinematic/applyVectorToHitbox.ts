import IHitbox from '../../models/IHitbox';
import ISpeed from '../../models/ISpeed';
import { IVector } from '../../models/IVector';

const applyVectorToHitbox = (
  hitbox: IHitbox,
  speed: ISpeed,
): IVector[] => {

  return hitbox.positions.map(pos => {
    return {
      x1: pos.x,
      y1: pos.y,
      x2: pos.x + speed.x,
      y2: pos.y + speed.y,
    };
  });

}

export default applyVectorToHitbox;