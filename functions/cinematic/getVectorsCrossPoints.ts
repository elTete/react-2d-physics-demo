import ICloseCollider from '../../models/ICloseCollider';
import ICollision from '../../models/ICollision';
import { IHitboxPos } from '../../models/IPosition';
import { IVector } from '../../models/IVector';
import getCrossLineDot from './getCrossLineDot';
import getCrossLineDotStrict from './getCrossLineDotStrict';
import getHitboxSegments from './getHitboxSegments';


const getVectorsCrossPoints = (
  colliders: ICloseCollider[],
  mcVectors: IVector[],
  mcKey: string,
  strict = false,
) => {
  return colliders.reduce(
    (accumulator: ICollision[], cc): ICollision[] => {
      const ccSegments = getHitboxSegments(cc.hitbox);
      const res: ICollision[] = [];

      if (cc.movingColliderKey !== mcKey) return accumulator;

      mcVectors.forEach(v => {
        ccSegments.forEach(seg => {
          const pos = strict ? getCrossLineDotStrict(v, seg) : getCrossLineDotStrict(v, seg);
          if (pos.x !== null && pos.y !== null) res.push({
            vector: v,
            position: <IHitboxPos>pos,
            segment: seg,
            hitboxTypes: cc.hitbox.types,
          });

        });
      });

      if (res.length) {
        return [...accumulator, ...<ICollision[]>res];
      }

      return accumulator;
    }
  , []);
}

export default getVectorsCrossPoints;