import IHitbox from '../../models/IHitbox';
import { IHitboxVec, IVector } from '../../models/IVector';


const getHitboxSegments = (hitbox: IHitbox) => {

  return hitbox.positions.reduce(
    (acc: IHitboxVec[], pos, i, positions): IHitboxVec[] => {
      if (i !== positions.length - 1) {
        const types = pos.types.length ? pos.types.filter(type => positions[i + 1].types.includes(type)) : hitbox.types;
        return [
          ...acc,
          {
            x1: pos.x,
            x2: positions[i + 1].x,
            y1: pos.y,
            y2: positions[i + 1].y,
            types,
          }
        ];
      } else {
        const types = pos.types.filter(type => positions[0].types.includes(type));
        return [
          ...acc,
          {
            x1: pos.x,
            x2: positions[0].x,
            y1: pos.y,
            y2: positions[0].y,
            types,
          }
        ]
      }

      return acc;
    }
  , []);
}

export default getHitboxSegments;