import { IVector } from '../../models/IVector';


const getAngleFromOx = (AB: IVector) => {
  let isHorizontal = false;
  let isVertical = false;
  let teta = 0;

  if (AB.y2 - AB.y1 === 0) {
    isHorizontal = true;
  } else if (AB.x2 - AB.x1 === 0) {
    isVertical = true;
  } else {
    const tanTeta = (AB.y2 - AB.y1) / (AB.x2 - AB.x1);
    teta = Math.atan(tanTeta);
  }

  return {
    isHorizontal,
    isVertical,
    teta,
  };
};

export default getAngleFromOx;