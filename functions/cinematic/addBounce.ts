import ISpeed from '../../models/ISpeed';
import { IVector } from '../../models/IVector';
import IVector0 from '../../models/IVector0';


const addBounce = (
  movingColliderRad: number,
  collidedSegmentRad: number,
  vector: IVector,
  speed: ISpeed,
  vectorToRedirectNorm: IVector0,
): {
  vector: IVector,
  speed: ISpeed,
} => {
  const rad = movingColliderRad + collidedSegmentRad;
  const newRad = Math.PI * 2 - movingColliderRad + collidedSegmentRad;

  const xCoef = Math.cos(newRad) / Math.cos(rad);
  const yCoef = Math.sin(newRad) / Math.sin(rad);

  return {
    vector: {
      ...vector,
      x2: vector.x1 + vectorToRedirectNorm.x * xCoef,
      y2: vector.y2 + vectorToRedirectNorm.y * yCoef,
    },
    speed: {
      x: speed.x * Math.sign(xCoef),
      y: speed.y * Math.sign(yCoef),
    },
  };
}

export default addBounce;