import { FunctionComponent, ReactNode, useEffect, useRef, useState } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import getRectHitBox from '../../functions/cinematic/getRectHitBox';
import IHitbox from '../../models/IHitbox';
import { HitboxType } from '../../models/IPosition';
import { RootState } from '../../store';
import { addCloseCollider, removeCloseCollider } from '../../store/features/game/gameSlice';
import { useAppDispatch } from '../../store/hooks';


const mapStateToProps = (state: RootState) => {
  return {
    movingColliders: state.game.movingColliders,
  };
}

const connector = connect(mapStateToProps);

type IPropsFromRedux = ConnectedProps<typeof connector>;

interface IProps extends IPropsFromRedux {
  style: any,
  CKey: string,
  children?: ReactNode | undefined,
  types?: HitboxType[],
}

// TODO: to fix : colliding does not work on x = 0 or y = 0
const Collider: FunctionComponent<IProps> = ({ movingColliders, style, CKey, children, types = [] }) => {
  const [hitbox, setHitbox] = useState<IHitbox>({
    key: CKey,
    positions: [],
    types,
  });
  const [closeMovingColliders, setCloseMovingColliders] = useState<string[]>([]);

  let xMin = 100000, xMax = -100000, yMin = 100000, yMax = -100000;
  hitbox.positions.forEach(pos => {
    if (pos.x < xMin) xMin = pos.x - 5000;
    if (pos.x > xMax) xMax = pos.x + 5000;
    if (pos.y < yMin) yMin = pos.y - 5000;
    if (pos.y > yMax) yMax = pos.y + 5000;
  });

  const ref = useRef<HTMLDivElement>(null);

  const dispatch = useAppDispatch();
  // const movingColliders = useAppSelector(state => state.game.movingColliders);

  useEffect(() => {
    const cmcs: string[] = closeMovingColliders;
    movingColliders.forEach(mc => {
      const found = mc.positions.find(mcPos => {
        return (
          mcPos.x > xMin &&
          mcPos.x < xMax &&
          mcPos.y > yMin &&
          mcPos.y < yMax
        );
      });

      if (found && !cmcs.find(c => c === mc.key)) {
        cmcs.push(mc.key);
        setCloseMovingColliders([...closeMovingColliders, mc.key]);
        dispatch(addCloseCollider({
          closeColliderKey: hitbox.key,
          isMoving: false,
          movingColliderKey: mc.key,
          hitbox,
        }));
      } else if (!found && cmcs.find(cmc => cmc === mc.key)) {
        setCloseMovingColliders(pre => {
          return pre.filter(cmc => cmc !== mc.key);
        });
        dispatch(removeCloseCollider({
          closeColliderKey: hitbox.key,
          isMoving: false,
          movingColliderKey: mc.key,
          hitbox,
        }));
      }

      if (cmcs !== closeMovingColliders) setCloseMovingColliders(cmcs);

    });
  }, [movingColliders]);

  useEffect(() => {
    if (ref.current) {
      const rect = ref.current.getBoundingClientRect();
      setHitbox(getRectHitBox(
        hitbox.key,
        hitbox.types,
        {
          x: rect.x + rect.width / 2,
          y: rect.y + rect.height / 2,
        },
        rect.width,
        rect.height,
      ));
    }
  }, [style]);

  return <div
    ref={ref}
   style={{ ...style }}
   className={CKey}
  >
    {children}
  </div>
}

export default connector(Collider);