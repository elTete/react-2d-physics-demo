import React, { CSSProperties, FunctionComponent, useEffect, useRef, useState } from 'react';
import { KeyOutline } from 'react-ionicons';

import MCType from '../../models/enums/MCType';
import IControls from '../../models/IControls';
import { HitboxType } from '../../models/IPosition';
// import { updateMouseSpeed } from '../../functions/cinematic/updateMouseSpeed';
import { incrementMouseUp, updateMouseSpeed, updateScroll } from '../../store/features/game/gameSlice';
import { useAppDispatch } from '../../store/hooks';
import Collider from '../Collider/Collider';
import CharMovingCollider from '../MovingCollider/CharMovingCollider/CharMovingCollider';
import { MovingCollider } from '../MovingCollider/MovingCollider';

import styles from './Lock.module.scss';

declare global {
  interface Window {
    controls: IControls,
  }
}

type LockProps = {
  
}

// TODO: this should be the redux Provider
export const Lock: FunctionComponent<LockProps> = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [gameStyle, setGameStyle] = useState<CSSProperties>({
    boxSizing: 'border-box',
    top: 1,
    left: 1,
    position: 'absolute',
  });

  const dispatch = useAppDispatch();

  const handleMouseUp = () => {
    dispatch(incrementMouseUp());
  }

  useEffect(() => {
    window.addEventListener('scroll', () => dispatch(
      updateScroll({ x: window.scrollX, y: window.scrollY })
    ));
    setGameStyle((pre => {
      return {
        ...pre,
        width: document.body.clientWidth - (gameStyle.left as number),
        height: document.documentElement.clientHeight - (gameStyle.top as number),
      }
    }))
    setIsLoading(false);
  }, []);

  if (isLoading) return <>
  </>;

  if ((gameStyle.width as number) < 800) {
    return <div
      className={styles.safe_container}
      // style={{ position: 'absolute' }}
      // onScroll={() => dispatch(updateScroll({ x: window.scrollX, y: window.scrollY }))}
      onMouseUp={handleMouseUp}
      onMouseMove={
        (e) => dispatch(updateMouseSpeed({ pageX: e.pageX, pageY: e.pageY }))
      }
      onTouchMove={
        (e) => dispatch(updateMouseSpeed({ pageX: e.touches[0].pageX, pageY: e.touches[0].pageY }))
      }
      onTouchStart={
        (e) => dispatch(updateMouseSpeed({ pageX: e.touches[0].pageX, pageY: e.touches[0].pageY }))
      }
      onTouchEnd={handleMouseUp}
    >

      <Collider style={gameStyle} CKey='game-board'>
        <div style={{ width: 200, height: 100, display: 'flex', top: 10, position: 'relative' }}>
          <MovingCollider
            behavior={HitboxType.Normal}
            frictionOn={true}
            gravityOn={true}
            MCKey='normal-woman'
            type={MCType.Text}
            content='touch and drag'
          />
          <MovingCollider
            behavior={HitboxType.Normal}
            frictionOn={true}
            gravityOn={true}
            MCKey='normal-woman'
            type={MCType.Text}
            content='touch and drag'
          />
          <MovingCollider
            behavior={HitboxType.Normal}
            frictionOn={true}
            gravityOn={true}
            MCKey='normal-woman'
            type={MCType.Text}
            content='touch and drag'
          />
          <MovingCollider
            behavior={HitboxType.Normal}
            frictionOn={true}
            gravityOn={true}
            MCKey='normal-woman'
            type={MCType.Text}
            content='touch and drag'
          />
          {/* <MovingCollider
            behavior={HitboxType.Bounce}
            frictionOn={true}
            gravityOn={true}
            MCKey='bounce-girl'
            type={MCType.Text}
            content='bounce'
          /> */}
          {/* <MovingCollider
            behavior={HitboxType.Bounce}
            frictionOn={false}
            gravityOn={false}
            MCKey='space-girl'
            type={MCType.Text}
            content='space'
          /> */}
        </div>

        <Collider
          style={{
            boxSizing: 'border-box',
            left: 50,
            backgroundColor: '#c5e3ff',
            padding: 10,
            fontSize: 10
          }}
          CKey='collider-1'
          types={[]}
        >
        <p>
          Moteur physique 2D réalisé avec React, Redux et Next.
          Ce projet a été pensé pour un affichage sur ordinateur bien qu&apos;il fonctionne sur mobile.
          Je vous invite à revenir sur cette page sur ordinateur portable ou de bureau.
        </p>
        <p>
          Touch, drag and throw the bloc above
        </p>
        </Collider>

        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'space-evenly',
            minHeight: 100,
            marginTop: 80,
          }}
        >
          <Collider
            style={{
              boxSizing: 'border-box',
              backgroundColor: '#c5e3ff',
              padding: 20,
              marginLeft: 90,
              marginBottom: 60,
              width: 'fit-content',
            }}
            CKey='collider-1'
            types={[]}
          >
            <p>
              Je suis un collider
            </p>
          </Collider>
          <Collider
            style={{
              boxSizing: 'border-box',
              backgroundColor: '#abf0d1',
              padding: 20,
              width: 'fit-content',
              marginLeft: 70,
            }}
            CKey='collider-1'
            types={[HitboxType.Bounce]}
          >
            <p>
              Je suis un collider avec rebond
            </p>
          </Collider>
        </div>

      </Collider>
    </div>
  }
  
  return <div
    className={styles.safe_container}
    // style={{ position: 'absolute' }}
    // onScroll={() => dispatch(updateScroll({ x: window.scrollX, y: window.scrollY }))}
    onMouseUp={handleMouseUp}
    onMouseMove={
      (e) => dispatch(updateMouseSpeed({ pageX: e.pageX, pageY: e.pageY }))
    }
    onTouchMove={
      (e) => dispatch(updateMouseSpeed({ pageX: e.touches[0].pageX, pageY: e.touches[0].pageY }))
    }
    onTouchStart={
      (e) => dispatch(updateMouseSpeed({ pageX: e.touches[0].pageX, pageY: e.touches[0].pageY }))
    }
    onTouchEnd={handleMouseUp}
  >

    <Collider style={gameStyle} CKey='game-board'>
      <div style={{ width: 150, height: 100, display: 'flex', top: 10, position: 'relative' }}>
        <MovingCollider
          behavior={HitboxType.Normal}
          frictionOn={true}
          gravityOn={true}
          MCKey='normal-woman'
          type={MCType.Text}
          content='click and drag'
        />
        <MovingCollider
          behavior={HitboxType.Normal}
          frictionOn={true}
          gravityOn={true}
          MCKey='normal-woman1'
          type={MCType.Text}
          content='click and drag'
        />
        <MovingCollider
          behavior={HitboxType.Normal}
          frictionOn={true}
          gravityOn={true}
          MCKey='normal-woman2'
          type={MCType.Text}
          content='click and drag'
        />
        {/* <MovingCollider
          behavior={HitboxType.Bounce}
          frictionOn={true}
          gravityOn={true}
          MCKey='bounce-girl'
          type={MCType.Text}
          content='bounce'
        /> */}
        {/* <MovingCollider
          behavior={HitboxType.Bounce}
          frictionOn={false}
          gravityOn={false}
          MCKey='space-girl'
          type={MCType.Text}
          content='space'
        /> */}
      </div>

      <Collider
        style={{
          boxSizing: 'border-box',
          left: 50,
          backgroundColor: '#c5e3ff',
          padding: 20,
        }}
        CKey='collider-1'
        types={[]}
      >
        <p>
          Moteur physique 2D réalisé avec React, Redux et Next.
          Ce n&apos;est qu&apos;un prototype que j&apos;ai créé pour passer le temps,
          il est possible de rencontrer des bugs.
        </p>
        <p>
          Vous pouvez lancez le bloc situé en haut à gauche à l&apos;aide de la souris.
          (click, drag and throw)
        </p>
        <p>
          Vous pouvez diriger le bloc key avec les touches directionnelles
          et la barre espace pour sauter.
        </p>
        <p>
          Certains blocs sont enregistrés comme collider et d&apos;autres non,
          les blocs en mouvement entrent en collision avec les colliders.
        </p>
        <p>
          Les blocs déplacés avec la souris n&apos;entrent pas en contact avec les colliders
          tant qu&apos;ils nont pas été lancés.
        </p>
        <p>
          Redimensionner la fenêtre ne met pas à jour les hitbox pour l&apos;instant,
          si vous redimensionnez la fenêtre, rechargez la page.
        </p>
      </Collider>

      <div
        style={{
          display: 'flex',
          justifyContent: 'space-evenly',
          minHeight: 100,
          marginTop: 30,
        }}
      >
        <Collider
          style={{
            boxSizing: 'border-box',
            backgroundColor: '#c5e3ff',
            padding: 20,
          }}
          CKey='collider-1'
          types={[]}
        >
          <p>
            Je suis un collider
          </p>
        </Collider>
        <div style = {{ padding: 20, backgroundColor: '#e8e9ee'}}>
          <p>Je ne suis pas un collider</p>
        </div>
        <Collider
          style={{
            boxSizing: 'border-box',
            backgroundColor: '#abf0d1',
            padding: 20,
          }}
          CKey='collider-1'
          types={[HitboxType.Bounce]}
        >
          <p>
            Je suis un collider avec rebond
          </p>
        </Collider>
        <Collider
          style={{
            boxSizing: 'border-box',
            backgroundColor: '#ff8484',
            padding: 20,
          }}
          CKey='collider-1'
          types={[HitboxType.Bounce, HitboxType.Spike]}
        >
          <p>
            Je suis un collider avec dégât
          </p>
        </Collider>
      </div>

      <CharMovingCollider
        behavior={HitboxType.Normal}
        frictionOn={true}
        gravityOn={true}
        MCKey='test-man'
        type={MCType.Text}
        content='key'
        firstPos={{
          x: 100,
          y: (gameStyle.height as number) - 100,
        }}
      />

      <div
        style={{
          bottom: 0,
          left: 150,
          position: 'absolute',
          width: (gameStyle.width as number - 150)
        }}
      >
        <div
          style={{
            display: 'flex',
            justifyContent: 'space-evenly',
            height: 60,
          }}
        >
          <Collider
            style={{
              boxSizing: 'border-box',
              backgroundColor: '#c5e3ff',
              paddingLeft: 50,
              paddingRight: 50,
            }}
            CKey='collider-1'
            types={[]}
          >
          </Collider>
          <Collider
            style={{
              boxSizing: 'border-box',
              backgroundColor: '#c5e3ff',
              paddingLeft: 50,
              paddingRight: 50,
            }}
            CKey='collider-1'
            types={[]}
          >
          </Collider>
          <Collider
            style={{
              boxSizing: 'border-box',
              backgroundColor: '#c5e3ff',
              paddingLeft: 50,
              paddingRight: 50,
            }}
            CKey='collider-1'
            types={[]}
          >
          </Collider>
          <Collider
            style={{
              boxSizing: 'border-box',
              backgroundColor: '#c5e3ff',
              paddingLeft: 50,
              paddingRight: 50,
            }}
            CKey='collider-1'
            types={[]}
          >
          </Collider>
          <Collider
            style={{
              boxSizing: 'border-box',
              backgroundColor: '#c5e3ff',
              paddingLeft: 50,
              paddingRight: 50,
            }}
            CKey='collider-1'
            types={[]}
          >
          </Collider>
          <Collider
            style={{
              boxSizing: 'border-box',
              backgroundColor: '#c5e3ff',
              paddingLeft: 50,
              paddingRight: 50,
            }}
            CKey='collider-1'
            types={[]}
          >
          </Collider>
        </div>
        <div style={{ height: 60 }}>

        </div>
        <div
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            height: 60,
          }}
        >
          <Collider
            style={{
              boxSizing: 'border-box',
              backgroundColor: '#c5e3ff',
              paddingLeft: 50,
              paddingRight: 50,
            }}
            CKey='collider-1'
            types={[]}
          >
          </Collider>
          <Collider
            style={{
              boxSizing: 'border-box',
              backgroundColor: '#c5e3ff',
              paddingLeft: 50,
              paddingRight: 50,
            }}
            CKey='collider-1'
            types={[]}
          >
          </Collider>
          <Collider
            style={{
              boxSizing: 'border-box',
              backgroundColor: '#c5e3ff',
              paddingLeft: 50,
              paddingRight: 50,
            }}
            CKey='collider-1'
            types={[]}
          >
          </Collider>
          <Collider
            style={{
              boxSizing: 'border-box',
              backgroundColor: '#c5e3ff',
              paddingLeft: 50,
              paddingRight: 50,
            }}
            CKey='collider-1'
            types={[]}
          >
          </Collider>
          <Collider
            style={{
              boxSizing: 'border-box',
              backgroundColor: '#c5e3ff',
              paddingLeft: 50,
              paddingRight: 50,
            }}
            CKey='collider-1'
            types={[]}
          >
          </Collider>
          <Collider
            style={{
              boxSizing: 'border-box',
              backgroundColor: '#c5e3ff',
              paddingLeft: 50,
              paddingRight: 50,
            }}
            CKey='collider-1'
            types={[]}
          >
          </Collider>
        </div>
        <div
          style={{ height: 60 }}
        >

        </div>
        <div style={{ display: 'flex', justifyContent: 'stretch' }}>
          <Collider
              style={{
                boxSizing: 'border-box',
                backgroundColor: '#c5e3ff',
                padding: 30,
              }}
              CKey='collider-1'
              types={[]}
            >
          </Collider>
          <Collider
            style={{
              boxSizing: 'border-box',
              backgroundColor: '#ff8484',
              padding: 30,
              width: (gameStyle.width as number) - 160
            }}
            CKey='collider-1'
            types={[HitboxType.Bounce, HitboxType.Spike]}
          >
          </Collider>
        </div>
      </div>

    </Collider>
  </div>
}