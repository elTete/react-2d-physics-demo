import {
  FunctionComponent,
  MouseEvent,
  useEffect,
  useRef,
  useState
} from 'react';
import addScrollToPos from '../../functions/cinematic/addScrollToPos';
import getRectHitBox from '../../functions/cinematic/getRectHitBox';
import { unlockPosition } from '../../functions/DOM/unlockPosition';
import useFall from '../../hooks/cinematicHooks/useFall';

import MCType from '../../models/enums/MCType';
import { HitboxType } from '../../models/IPosition';
import { updateMovingCollider } from '../../store/features/game/gameSlice';
import { useAppDispatch, useAppSelector } from '../../store/hooks';

import styles from './MovingCollider.module.scss';

interface IProps {
  gravityOn: boolean,
  frictionOn: boolean,
  behavior: HitboxType,
  MCKey: string,
  type: MCType,
  content?: string,
};

export const MovingCollider: FunctionComponent<IProps> = ({
  gravityOn,
  frictionOn,
  behavior,
  MCKey,
  type,
  content,
}) => {
  const [position, setPosition] = useState({ x: 0, y: 0 });
  const [originalPos, setOriginalPos] = useState({ x: 0, y: 0 });
  const [parentPos, setParentPos] = useState({x: 0, y: 0});
  const [dragged, setDragged] = useState(false);
  const [life, setLife] = useState(1);
  const width = 50;
  const height = 50;

  const ref = useRef<HTMLDivElement>(null);
  
  const hitbox = getRectHitBox(
    MCKey,
    [],
    { x: position.x, y: position.y },
    width,
    height,
  )

  const dispatch = useAppDispatch();
  const { mousePos, scroll, mouseUpCount } = useAppSelector(state => state.game);

  const { triggerFall, resetFall, spike } = useFall({
    position,
    setPosition,
    movingColliderKey: MCKey,
    hitbox,
    gravityOn,
    behavior,
    frictionOn,
  });

  useEffect(() => {
    const rect = ref.current?.getBoundingClientRect();
    if (rect) setOriginalPos({ x: rect.x + width/2, y: rect.y + height/2 });
  }, []);

  useEffect(() => {
    if (spike) {
      setLife(life - 1);
      if (life - 1 <= 0) {
        setPosition(originalPos);
        setLife(1);
        resetFall();
      }
    }
  }, [spike]);


  const handleMouseDown = (e: any) => {
    e.preventDefault()
    unlockPosition(e);
    setPosition({
      x: mousePos.x2,
      y: mousePos.y2,
    });
    setDragged(true);
  }

  useEffect(() => {
    if (dragged) {
      setDragged(false);
      triggerFall();
    }
  }, [mouseUpCount]);

  useEffect(() => {
    if (dragged) {
      window.requestAnimationFrame(() => {
        setPosition({
          x: mousePos.x2,
          y: mousePos.y2,
        });
      });
    }
  }, [mousePos.x2, mousePos.y2]);

  useEffect(() => {
    const parentRect = ref.current?.parentElement?.getBoundingClientRect();
    if (
      parentRect &&
      (
        parentRect.x !== parentPos.x ||
        parentRect.y !== parentPos.y
      )
    ) setParentPos(addScrollToPos({ x: parentRect.x, y: parentRect.y }, scroll));

    dispatch(updateMovingCollider(getRectHitBox(
      MCKey,
      [],
      { x: position.x, y: position.y },
      width,
      height,
    )));
  }, [position.x, position.y]);

  if (type === MCType.Img) return <img
    src="/noun-key.svg"
    className='test'
    onMouseDown={handleMouseDown}
    onTouchStart={handleMouseDown}
    color={'#00000'}
    height={`${height}px`}
    width={`${width}px`}
    style={{ left: position.x - width / 2, top: position.y - height / 2, border: 'solid black 3px'}}
  />

  if (type === MCType.Text && content) return <div
    ref={ref}
    className={MCKey + ' ' + styles.moving_collider}
    onMouseDown={handleMouseDown}
    onTouchStart={handleMouseDown}
    color={'#00000'}
    style={{ left: position.x - width / 2 - parentPos.x, top: position.y - height / 2 - parentPos.y, border: 'solid black 1px', width, height, fontSize: 12, boxSizing: 'border-box' }}
  >
    {content}
  </div>

  return <></>
}