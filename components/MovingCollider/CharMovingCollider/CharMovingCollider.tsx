import {
  FunctionComponent,
  KeyboardEvent,
  useEffect,
  useRef,
  useState
} from 'react';
import { connect, ConnectedProps } from 'react-redux';
import addScrollToPos from '../../../functions/cinematic/addScrollToPos';
import getRectHitBox from '../../../functions/cinematic/getRectHitBox';
import { unlockPosition } from '../../../functions/DOM/unlockPosition';
import useCharacter from '../../../hooks/cinematicHooks/useCharacter';
import useKeyPress from '../../../hooks/useKeyPress';

import MCType from '../../../models/enums/MCType';
import IControls from '../../../models/IControls';
import { HitboxType, IPosition } from '../../../models/IPosition';
import ISpeed from '../../../models/ISpeed';
import { RootState } from '../../../store';
import { updateMovingCollider } from '../../../store/features/game/gameSlice';
import { useAppDispatch, useAppSelector } from '../../../store/hooks';

import styles from '../MovingCollider.module.scss';

// const mapStateToProps = (state: RootState) => {
//   return {
//     controls: state.game.controls,
//   };
// }

// const connector = connect(mapStateToProps);

// type IPropsFromRedux = ConnectedProps<typeof connector>

interface IProps {
  gravityOn: boolean,
  frictionOn: boolean,
  behavior: HitboxType,
  MCKey: string,
  type: MCType,
  content?: string,
  firstPos: { x: number, y: number}
};

const CharMovingCollider: FunctionComponent<IProps> = ({
  gravityOn,
  frictionOn,
  behavior,
  MCKey,
  type,
  content,
  firstPos
}) => {
  const [position, setPosition] = useState(firstPos);
  const [originalPos, setOriginalPos] = useState(firstPos);
  const [parentPos, setParentPos] = useState({x: 0, y: 0});
  const [life, setLife] = useState(1);
  // const [move, setMove] = useState({ x: 0, y: 0 });
  const width = 50;
  const height = 50;

  const ref = useRef<HTMLDivElement>(null);
  
  const hitbox = getRectHitBox(
    MCKey,
    [],
    { x: position.x, y: position.y },
    width,
    height,
  );

  const dispatch = useAppDispatch();
  const { scroll } = useAppSelector(state => state.game);

  const controls = useKeyPress();

  const getMove = (controls: IControls) => {
    let xMove = 0;
    let yMove = 0;
    if (controls.left) xMove -= 10;
    if (controls.right) xMove += 10;
    if (controls.jump) yMove -= 70;

    return {
      x: xMove,
      y: yMove,
    };
  }

  const move = getMove(controls);

  // useEffect(() => {
  //   const newMove = getMove(controls);
  //   if (newMove.x !== move.x || newMove.y !== move.y) {
  //     setMove(getMove(controls));
  //   }
  // }, [controls]);

  const { triggerFall, resetFall, spike } = useCharacter({
    position,
    setPosition,
    movingColliderKey: MCKey,
    hitbox,
    gravityOn,
    behavior,
    frictionOn,
    move,
  });

  // useEffect(() => {
  //   const rect = ref.current?.getBoundingClientRect();
  //   if (rect) setOriginalPos({ x: rect.x + width/2, y: rect.y + height/2 });
  // }, []);

  useEffect(() => {
    if (spike) {
      setLife(life - 1);
      if (life - 1 <= 0) {
        setPosition(originalPos);
        setLife(1);
        resetFall();
      }
    }
  }, [spike]);

  useEffect(() => {
    const parentRect = ref.current?.parentElement?.getBoundingClientRect();
    if (
      parentRect &&
      (
        parentRect.x !== parentPos.x ||
        parentRect.y !== parentPos.y
      )
    ) setParentPos(addScrollToPos({ x: parentRect.x, y: parentRect.y }, scroll));

    dispatch(updateMovingCollider(getRectHitBox(
      MCKey,
      [],
      { x: position.x, y: position.y },
      width,
      height,
    )));
  }, [position.x, position.y]);

  // useEffect(() => {
  //   window.requestAnimationFrame(triggerFall);
  // }, [controls.up, controls.down, controls.left, controls.right, controls.jump]);
  useEffect(() => {
    if (life > 0) triggerFall();
  }, [controls]);

  if (type === MCType.Text && content) return <div
    ref={ref}
    className={MCKey + ' ' + styles.moving_collider}
    color={'#00000'}
    style={{ position: 'absolute', left: position.x - width / 2 - parentPos.x, top: position.y - height / 2 - parentPos.y, border: 'solid black 1px', width, height, fontSize: 12, boxSizing: 'border-box' }}
  >
    {content}
  </div>

  return <></>
}

export default CharMovingCollider;