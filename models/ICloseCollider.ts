import IHitbox from './IHitbox';


export default interface ICloseCollider {
  isMoving: boolean;
  closeColliderKey: string;
  movingColliderKey: string;
  hitbox: IHitbox;
}