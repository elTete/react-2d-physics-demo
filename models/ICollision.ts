import { HitboxType, IHitboxPos } from './IPosition';
import { ISplitVector, IVector } from './IVector';


/**
 * infos about a collision
 */
export default interface ICollision {
  vector: IVector,
  position: IHitboxPos,
  segment: IVector,
  hitboxTypes: HitboxType[],
}

/**
 * @inheritdoc
 * after calculations
 */
export interface IExtendedCol extends ICollision {
  vSplitted: ISplitVector,
  hitTypes: HitboxType[],
}

// export interface IClosestCol extends ICalculatedCol {
//   vSplitted: ISplitVector,
//   /** other collisions */
//   secondClosest?: ICalculatedCol,
// }