

export default interface ISpeedAcc {
  x: number;
  y: number;
};