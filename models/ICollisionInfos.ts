import { IHitboxPos, IPosition } from './IPosition';
import { IVector } from './IVector';

export default interface ICollisionInfos {
  deltaX: number;
  deltaY: number;
  vector: IVector;
  segment: IVector;
  position: IHitboxPos;
}