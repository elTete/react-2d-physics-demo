export interface IPosition {
  x: number;
  y: number;
}

export enum HitboxType {
  Bounce = 'BOUNCE',
  Normal = 'NORMAL',
  Glue = 'GLUE',
  Spike = 'SPIKE',
}

export interface IHitboxPos extends IPosition {
  types: HitboxType[],
}