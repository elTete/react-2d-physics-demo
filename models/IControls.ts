


export default interface IControls {
  up: boolean,
  down: boolean,
  left: boolean,
  right: boolean,
  jump: boolean,
}