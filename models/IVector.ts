import { HitboxType, IPosition } from './IPosition';


export interface IVector {
  x1: number;
  x2: number;
  y1: number;
  y2: number;
}

export interface IHitboxVec extends IVector{
  types: HitboxType[],
}

/**
 * vector with infos about norms
 */
export interface IExtendedV extends IVector {
  norm: number,
  normXY: IPosition,
}

/**
 * splitted vector that went through a collider's segment
 */
export interface ISplitVector {
  /** vector part that went through collider's segment */
  tresspasser: IExtendedV,
  /** vector part that did not went through collider's segment */
  rightful: IExtendedV,
}