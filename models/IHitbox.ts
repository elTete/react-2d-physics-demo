import { HitboxType, IHitboxPos } from './IPosition';


export default interface IHitbox {
  key: string,
  positions: IHitboxPos[],
  types: HitboxType[],
}
