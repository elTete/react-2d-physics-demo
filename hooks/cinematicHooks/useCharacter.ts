import { Dispatch, SetStateAction, useEffect, useRef, useState } from 'react';
import addFall from '../../functions/cinematic/addFall';
import addHit from '../../functions/cinematic/addHit';
import getLowestPoint from '../../functions/cinematic/getLowestPoint';
import xIsBetween from '../../functions/cinematic/isBetween';
import ICollision from '../../models/ICollision';

import IHitbox from '../../models/IHitbox';
import { HitboxType, IPosition } from '../../models/IPosition';
import { useAppSelector } from '../../store/hooks';


interface ISpeed {
  x: number;
  y: number;
};

/**
 * sign
 */
interface ISpeedAcc {
  x: number;
  y: number;
}

interface IArguments {
  position: IPosition,
  setPosition: Dispatch<SetStateAction<IPosition>>,
  movingColliderKey: string,
  isCharacter?: boolean,
  hitbox: IHitbox,
  gravityOn : boolean,
  frictionOn: boolean,
  behavior: HitboxType,
  move: IPosition,
}

/**
 * 
 * @param fallOn when true component falls until it reaches a collider
 * @param position 
 * @param setPosition 
 */
 const useCharacter = ({
   position,
   setPosition,
   movingColliderKey,
   hitbox,
   gravityOn,
   frictionOn,
   behavior,
   move,
}: IArguments) => {
  const [fallCount, setFallCount] = useState(0);
  const [speed, setSpeed] = useState<ISpeed | null>(null);
  const [speedAcc, setSpeedAcc] = useState<ISpeedAcc>({ x: 0, y: 0 });
  const [date, setDate] = useState(0);
  const [stops, setStops] = useState(false);
  const [ground, setGround] = useState(false);
  const [spike, setSpike] = useState(false);

  const { closeColliders } = useAppSelector(state => state.game);

  useEffect(() => {
    let xMove: number = move.x;
    let yMove: number = move.y;

    // if (controls.right && !controls.left) {
    //   xMove = 10;
    // }
    // if (controls.left && !controls.right) {
    //   xMove = -10;
    // };
    // if (!controls.right && !controls.left || controls.right && controls.left) {
    //   xMove = 0;
    // };
    // if (controls.jump && ground) {
    //   yMove = -110;
    // }

    if (fallCount && !speed) {

      setStops(false);
      setSpeedAcc({
        x: Math.sign(xMove),
        y: -1,
      });
      setSpeed({
        x: xMove,
        y: ground ? yMove : 0,
      });
      setDate(Date.now());
      setFallCount(fallCount + 1);
    }

    if (
      fallCount &&
      speed &&
      !stops
    ) {
      const lowest = getLowestPoint(hitbox);
      let newXSpeed: number;
      if (move.x > 0) {
        if (speed.x < 0) {
          newXSpeed = speed.x + xMove;
        } else if (speed.x >= 0 && speed.x <= xMove) {
          newXSpeed = xMove;
        } else {
          newXSpeed = speed.x;
        };
      }
      else if (move.x < 0) {
        if (speed.x > 0) {
          newXSpeed = speed.x + xMove;
        } else if (speed.x <= 0 && speed.x >= xMove) {
          newXSpeed = xMove;
        } else {
          newXSpeed = speed.x;
        }
      }
      else newXSpeed = speed.x;
      // newXSpeed = speed.x + move.x;

      const newSpeed = {
        x: newXSpeed,
        y: yMove && ground ? yMove : speed.y,
      }
      const newSpeedAcc = {
        // ...speedAcc,
        x: Math.sign(newXSpeed),
        y: -1,
      }

      const fall = addFall(position, newSpeed, newSpeedAcc, date, gravityOn, frictionOn, ground);

      const hits = addHit(
        fall.vector,
        fall.speed,
        closeColliders,
        movingColliderKey,
        hitbox,
        fall.gravity * speedAcc.y,
        behavior,
      );
      const hit = hits.result[hits.result.length - 1];

      const newYSpeed = hit.speed.y + fall.gravity * speedAcc.y;

      requestAnimationFrame(() => {

        if (
          hit.col &&
          (lowest.point === hit.col.segment.y1 ||
          lowest.point === hit.col.segment.y2) &&
          (xIsBetween(lowest.seg.x1, hit.col.segment) ||
          xIsBetween(lowest.seg.x2, hit.col.segment) ||
          xIsBetween(hit.col.segment.x1, lowest.seg) ||
          xIsBetween(hit.col.segment.x2, lowest.seg))
        ) {
          setGround(true);
        } else {
          setGround(false);
        }
        if (hits.types.includes(HitboxType.Spike) && !spike) {
          setSpike(true);
        } else if (spike) {
          setTimeout(() => {
            setSpike(false);
          }, 500);
        }
        setPosition({ x: hit.vector.x2, y: hit.vector.y2 });
        setSpeed({
          x: hit.speed.x,
          y: newYSpeed,
        });
        // if (hit.hits) setSpeedAcc({ x: Math.sign(hit.speed.x), y: -1 });
        setFallCount(fallCount + 1);
        setStops(hit.stopped);

        setDate(Date.now());
      });
    } else if (
      stops &&
      // only on trigger touch to enable moving
      (move.x || move.y)
      // (
      //   (controls.left || controls.right) &&
      //   !controls.jump &&
      //   !(controls.left && controls.right)
      // ) ||
      // (controls.jump && ground &&
      // !controls.left && !controls.right)
    ) {
      window.requestAnimationFrame(() => {
        setStops(false);
        setFallCount(fallCount + 1);
      });
    } else if (fallCount) {
      requestAnimationFrame(() => {
        setDate(Date.now());
        setFallCount(0);
        setStops(true);
        setSpeed(null);
      });
    }

  }, [fallCount]);

  return {
    triggerFall: () => { if (!fallCount) setFallCount(1) },
    resetFall: () => setFallCount(0),
    spike,
  }
}

export default useCharacter;