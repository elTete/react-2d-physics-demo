import { Dispatch, SetStateAction, useEffect, useRef, useState } from 'react';
import addFall from '../../functions/cinematic/addFall';
import addHit from '../../functions/cinematic/addHit';

import IHitbox from '../../models/IHitbox';
import { HitboxType, IPosition } from '../../models/IPosition';
import { useAppSelector } from '../../store/hooks';


interface ISpeed {
  x: number;
  y: number;
};

/**
 * sign
 */
interface ISpeedAcc {
  x: number;
  y: number;
}

interface IArguments {
  position: IPosition,
  setPosition: Dispatch<SetStateAction<IPosition>>,
  movingColliderKey: string,
  hitbox: IHitbox,
  gravityOn : boolean,
  frictionOn: boolean,
  behavior: HitboxType,
}

/**
 * 
 * @param fallOn when true component falls until it reaches a collider
 * @param position 
 * @param setPosition 
 */
 const useFall = ({
   position,
   setPosition,
   movingColliderKey,
   hitbox,
   gravityOn,
   frictionOn,
   behavior,
}: IArguments) => {
  const [fallCount, setFallCount] = useState(0);
  const [speed, setSpeed] = useState<ISpeed | null>(null);
  const [speedAcc, setSpeedAcc] = useState<ISpeedAcc>({ x: 0, y: 0 });
  const [date, setDate] = useState(0);
  const [stops, setStops] = useState(false);
  const [spike, setSpike] = useState(false);

  const { movingColliders, closeColliders, mouseSpeed } = useAppSelector(state => state.game);

  useEffect(() => {

    if (fallCount && !speed) {
      const xSpeed = mouseSpeed.x;
      const ySpeed = mouseSpeed.y;

      setStops(false);
      setSpeedAcc({
        x: Math.sign(xSpeed),
        y: -1,
      });
      setSpeed({
        x: xSpeed,
        y: ySpeed,
      });
      setDate(Date.now());
      setFallCount(fallCount + 1);
    }

    if (
      fallCount &&
      speed &&
      !stops
    ) {

      const fall = addFall(position, speed, speedAcc, date, gravityOn, frictionOn);
      
      const hits = addHit(
        fall.vector,
        fall.speed,
        closeColliders,
        movingColliderKey,
        hitbox,
        fall.gravity * speedAcc.y,
        behavior,
      );
      const hit = hits.result[hits.result.length - 1];

      const newYSpeed = hit.speed.y + fall.gravity * speedAcc.y;

      requestAnimationFrame(() => {
        if (hits.types.includes(HitboxType.Spike) && !spike) {
          setSpike(true);
        } else if (spike) {
          setTimeout(() => {
            setSpike(false);
          }, 500);
        }
        if (hit.stopped) {
          setStops(true);
        }

        setPosition({ x: hit.vector.x2, y: hit.vector.y2 });
        setSpeed({
          x: hit.speed.x,
          y: newYSpeed,
        });
        if (hit.hits) setSpeedAcc({ x: Math.sign(hit.speed.x), y: -1 });
        setFallCount(fallCount + 1);
        setStops(hit.stopped);

        setDate(Date.now());
      });
    } else if (fallCount) {
      requestAnimationFrame(() => {
        setFallCount(0);
        setSpeed(null);
      });
    }

  }, [fallCount]);

  return {
    triggerFall: () => setFallCount(1),
    resetFall: () => setFallCount(0),
    spike,
  }
}

export default useFall;