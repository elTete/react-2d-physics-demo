import { useEffect, useRef, useState } from 'react';


const useKeyPress = () => {
  const [controls, setControls] = useState({
    up: false,
    down: false,
    left: false,
    right: false,
    jump: false,
  });

  useEffect(() => {
    window.controls = controls
  }, [controls]);

  useEffect(() => {
    window.addEventListener('keydown', (e) => {
      switch (e.key) {
        case 'ArrowUp':
          setControls({ ...window.controls, up: true });
          break;
        case 'ArrowDown':
          setControls({ ...window.controls, down: true });
          break;
        case 'ArrowLeft':
          setControls({ ...window.controls, left: true });
          break;
        case 'ArrowRight':
          setControls({ ...window.controls, right: true });
          break;
        case ' ':
          setControls({ ...window.controls, jump: true });
          break;
      }
    });
    window.addEventListener('keyup', (e) => {
      switch (e.key) {
        case 'ArrowUp':
          setControls({ ...window.controls, up: false });
          break;
        case 'ArrowDown':
          setControls({ ...window.controls, down: false });
          break;
        case 'ArrowLeft':
          setControls({ ...window.controls, left: false });
          break;
        case 'ArrowRight':
          setControls({ ...window.controls, right: false });
          break;
        case ' ':
          setControls({ ...window.controls, jump: false });
          break;
      }
    });
  }, []);

  return controls;
}

export default useKeyPress;