import { configureStore } from '@reduxjs/toolkit';
import collidersReducer from './features/game/gameSlice';


const store = configureStore({
  reducer: {
    game: collidersReducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
export default store;