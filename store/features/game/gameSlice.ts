import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { MutableRefObject } from 'react';
import { RootState } from '../..';
import getRectHitBox from '../../../functions/cinematic/getRectHitBox';
import ICloseCollider from '../../../models/ICloseCollider';
import IControls from '../../../models/IControls';
import IHitbox from '../../../models/IHitbox';
import { IPosition } from '../../../models/IPosition';
import { IVector } from '../../../models/IVector';
import IVector0 from '../../../models/IVector0';

interface GameState {
  movingColliders: IHitbox[],
  closeColliders: ICloseCollider[],
  scroll: IPosition,
  mouseUpCount: number,
  mousePos: IVector,
  mouseSpeed: IVector0,
  controls: IControls,
  date: number,
};

const initialState: GameState =  {
  movingColliders: [],
  closeColliders: [],
  scroll: {
    x: 0,
    y: 0,
  },
  mouseUpCount: 0,
  mousePos: {
    x1: 0,
    y1: 0,
    x2: 0,
    y2: 0,
  },
  mouseSpeed: {
    x: 0,
    y: 0,
  },
  controls: {
    up: false,
    down: false,
    left: false,
    right: false,
    jump: false,
  },
  date: 0,
};

export const gameSlice = createSlice({
  name: 'game',
  initialState,
  reducers: {
    addCloseCollider: (
      state,
      action: PayloadAction<ICloseCollider>
    ) => {
      state.closeColliders = [...state.closeColliders, action.payload];
    },
    removeCloseCollider: (
      state,
      action: PayloadAction<ICloseCollider>
    ) => {
      const closeCollider = action.payload;
      state.closeColliders = state.closeColliders.filter(cc => {

        return (
          cc.closeColliderKey !== closeCollider.closeColliderKey ||
          cc.movingColliderKey !== closeCollider.movingColliderKey
        );
      });
    },
    updateMovingCollider: (
      state,
      action: PayloadAction<IHitbox>,
    ) => {
      const hitbox = action.payload;
      let modified = 0;
      const colliders = state.movingColliders.map(mc => {
        if (mc.key === hitbox.key) {
          modified++;
          mc.positions = hitbox.positions;
        }
        return mc;
      });
      if (modified === 1) {
        state.movingColliders = [...colliders];
      } else if (modified > 1) {
        console.error('more than one collider with the same key');
      } else {
        state.movingColliders = [...state.movingColliders, hitbox];
      }
    },
    incrementMouseUp: (state) => {
      state.mouseUpCount++;
    },
    updateMouseSpeed: (
      state,
      action: PayloadAction<{ pageX: number, pageY: number }>
    ) => {
      const now = Date.now();
      const date = state.date ? state.date : now;
      const coord = action.payload;
      const mousePos = state.mousePos;
      const pos = {
        x1: mousePos.x2 ? mousePos.x2 : coord.pageX,
        y1: mousePos.y2 ? mousePos.y2 : coord.pageY,
        x2: coord.pageX,
        y2: coord.pageY,
      }

      state.mousePos = pos;

      const deltaDate = now - date;
      const coef = 40 / deltaDate;
      const xSpeed = (pos.x2 - pos.x1) * coef;
      const ySpeed = (pos.y2 - pos.y1) * coef;

      state.mouseSpeed = {
        x: xSpeed,
        y: ySpeed,
      }

      state.date = now;
    },
    updateScroll: (
      state,
      action: PayloadAction<IPosition>
    ) => {
      state.scroll = action.payload;
    },
    handleKeyDown: (
      state,
      action: PayloadAction<string>,
    ) => {
      switch (action.payload) {
        case 'ArrowUp':
          state.controls.up = true;
          break;
        case 'ArrowDown':
          state.controls.down = true;
          break;
        case 'ArrowLeft':
          state.controls.left = true;
          break;
        case 'ArrowRight':
          state.controls.right = true;
          break;
        case ' ':
          state.controls.jump = true;
          break;
      }
    },
    handleKeyUp: (
      state,
      action: PayloadAction<string>
    ) => {
      switch (action.payload) {
        case 'ArrowUp':
          state.controls.up = false;
          break;
        case 'ArrowDown':
          state.controls.down = false;
          break;
        case 'ArrowLeft':
          state.controls.left = false;
          break;
        case 'ArrowRight':
          state.controls.right = false;
          break;
        case ' ':
          state.controls.jump = false;
      }
    },
  },
});

export const {
  updateMouseSpeed,
  updateMovingCollider,
  addCloseCollider,
  removeCloseCollider,
  updateScroll,
  incrementMouseUp,
  handleKeyDown,
  handleKeyUp,
} = gameSlice.actions;

export const selectGame = (state: RootState) => {
  state.game.movingColliders;
}

export default gameSlice.reducer;